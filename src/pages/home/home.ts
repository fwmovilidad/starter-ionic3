import { Component } from '@angular/core';
import { NavController } from "ionic-angular";
import { Page } from "../page/page";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  name: string = "HomePage";

  constructor(
    public nav: NavController
  ) { }

  goToPage(): void {
    this.onLogin();
    if (window.visualizarPaso) {
      window.visualizarPaso({
        type: 'event',
        evtCategory: "Button",
        evtAction: "Click",
        evtLabel: "goToPage2"
      });
    }
    this.nav.push(Page);
  }

  onLogin(): void {
    window.datalayer.idUser = 'exampleUserId';
  }

}
